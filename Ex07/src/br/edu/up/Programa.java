package br.edu.up;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Programa {

	public static void main(String[] args) throws FileNotFoundException {

		File arquivo = new File("C:\\_src\\ws-qui-man\\Ex07\\src\\alunos.csv");
		Scanner leitor = new Scanner(arquivo);
		leitor.nextLine(); //Descartar a primeira linha.
		
		Aluno[] alunos = new Aluno[3];
		
		int contador = 0;
		while(leitor.hasNext()) {
			String linha = leitor.nextLine();
			String[] dadosLinha = linha.split(";");
			String matricula = dadosLinha[0];
			String nome = dadosLinha[1];
			//Double.parseDouble faz a convers�o texto > n�mero
			double nota = Double.parseDouble(dadosLinha[2]);
			
			Aluno novoAluno = new Aluno();
			novoAluno.matricula = matricula;
			novoAluno.nome = nome;
			novoAluno.nota = nota;
			
			alunos[contador] = novoAluno;
					
//			System.out.println(linha);
			contador++;
		}
		
		System.out.println("Qtde de alunos: " + contador);
		
		for (int i = 0; i < alunos.length; i++) {
			Aluno a = alunos[i];
			System.out.println("Nome: " + a.nome);
		}
		
		
		leitor.close();
	}

}
